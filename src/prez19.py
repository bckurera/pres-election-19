import prez_2019_ed_res as db

FORMAT_F_NUM = "5,.2f"
FORMAT_I_NUM = "9,d"

##Find out the district in which SLPP secured the highest votes (precentage/ count)##
def find_max_votes_per_party(p):
    obj = db.prez_2019_ed_res()

    for party in p:
        valid_votes = obj.get_column("Valid")
        votes_for_slpp = obj.get_column(party)

        temp_prec = 0
        temp_count = 0
        temp_prec_dist = ""
        temp_count_dist = ""

        for x in votes_for_slpp.keys(): 
            p = votes_for_slpp[x] / valid_votes[x]

            if(p > temp_prec):
                temp_prec = p
                temp_prec_dist = x

            if(votes_for_slpp[x] > temp_count):
                temp_count = votes_for_slpp[x]
                temp_count_dist = x

        print(party, " highest %     - ", format(temp_prec*100, FORMAT_F_NUM) , " % -", temp_prec_dist)
        print(party, " highest count - ", format(temp_count, FORMAT_I_NUM) , " votes -", temp_count_dist)



##Highest voters - turnout##
def voters_prec():
    obj = db.prez_2019_ed_res()

    valid_dist = obj.get_column("Valid")
    rejected_dist = obj.get_column("Rejected")

    res = {}

    for x in valid_dist.keys():
        p = rejected_dist [x] / (valid_dist[x] + rejected_dist [x])
        p = round(p, 4)
        ext = {p: x}
        res.update(ext)

    for x in sorted(res.keys(), reverse = True):
        print(" Rejected % - ", format((x*100), FORMAT_F_NUM), " - ", res[x], " count - ", format(rejected_dist[res[x]], FORMAT_I_NUM))

##Two Party world##
def two_party_analysis():
    obj = db.prez_2019_ed_res()

    slpp_votes = obj.get_column("SLPP")
    ndf_votes = obj.get_column("NDF")
    others_vote = obj.get_column("Others")

    res = []

    for x in slpp_votes.keys():
        if(slpp_votes[x] < (ndf_votes[x] + others_vote[x])):
            res.append(x)

    for x in range(0, len(res)):
        dist = res[x]
        combined = (ndf_votes[dist] + others_vote[dist])
        print("Combinely Secured - ", dist, " combined count - ", (combined), " % - ", format((slpp_votes[dist]/(combined))*100,FORMAT_F_NUM ), " %")



party = ["SLPP", "NDF"]
print("\n----------------------------------")
find_max_votes_per_party(party)

print("\n----------------------------------")
voters_prec()

print("\n----------------------------------")
two_party_analysis()

