class prez_2019_ed_res:

    '''
    This class holds data organized per Electoral Districts
    Columns
    SLPP | NDF | Others | Valid | Rejected | Total | Electors
    '''

    districts = ["Colombo","Gampaha","Kalutara","Mahanuwara","Matale","NuwaraEliya","Galle","Matara","Hambantota","Jaffna","Vanni","Batticaloa","Digamadulla","Trincomalee","Kurunegala","Puttalam","Anuradhapura","Polonnaruwa","Badulla","Moneragala","Ratnapura","Kegalle"]

    results = [727713,559921,80543,1368177,15333,1383510,1670403,855870,494671,93259,1443800,15751,1459551,1751892,482920,284213,44630,811763,6847,818610,955080,471502,417355,46018,934875,9020,943895,1111860,187821,134291,17109,339221,3252,342473,401496,175823,277913,23128,476864,7155,484019,569028,466148,217401,41809,725358,5878,731236,858749,374481,149026,33361,556868,3782,560650,652417,278804,108906,33664,421374,3179,424553,485986,23261,312722,36930,372913,11251,384164,564984,26105,174739,11934,212778,3294,216072,282119,38460,238649,26112,303221,4258,307479,398301,135058,259673,16839,411570,3158,414728,503790,54135,166841,10434,231410,1832,233242,281114,652278,416961,57371,1126610,8522,1135132,1331705,230760,199356,23860,453976,4478,458454,599042,342223,202348,35775,580346,4916,585262,682450,147340,112473,18111,277924,2563,280487,326443,276211,251706,32428,560345,6978,567323,657766,208814,92539,18251,319604,3000,322604,366524,448044,264503,35124,747671,5853,753524,864979,320484,228032,27315,575831,5152,580983,676440]


    def get_res_by_ed(self, dist):

        '''
        This method returns a list - all data related to a district
        '''

        try:
            ind = self.districts.index(dist)

            x = 7 * ind
            output = [dist]
            output.append(self.results[x: x+7])
            return output

        except ValueError: 
            return 0

    def get_dist_list(self):

        '''
        This method returns a list - all district names
        '''

        return self.districts

    def get_column(self, col_name):

        '''
        This method returns a list - column data related to a dimension
        '''

        output = {}

        r = 0

        if(col_name == "NDF"):
            r = 1

        if(col_name == "Others"):
            r = 2
        
        if(col_name == "Valid"):
            r = 3

        if(col_name == "Rejected"):
            r = 4

        if(col_name == "Total"):
            r = 5

        if(col_name == "Electors"):
            r = 6

        i = 0
        j = 0
        while i < len(self.results):
            ext = {self.districts[j] : self.results[i + r]}
            output.update(ext)
            i = i + 7
            j = j + 1

        return output


    def get_res_by_ed_op(self, dist, option):

        '''
        This method returns a single value - intersection of a row and column
        '''

        try:
            ind = self.districts.index(dist)

            if(option == 'Rejected'):
                op = 4

            if(option == 'Valid'):
                op = 3

            if(option == 'Total'):
                op = 6

            x = (7 * ind) + op
            output = self.results[x: x+1]
            return output[0]

        except ValueError: 
            return 0
